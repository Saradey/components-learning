package com.evgeny.goncharov.lesson_1.viewModel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

/**
 * Created by Evgeny Goncharov on 22/12/2018.
 * jtgn@yandex.ru
 */

//фибрика ViewModel
public class ModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final int id;

    public ModelFactory(int id) {
        super();
        this.id = id;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == MyViewModel.class) {
            return (T) new MyViewModel();
        }
        return null;
    }


}
