package com.evgeny.goncharov.lesson_1.Room.entity;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Transaction;

import com.evgeny.goncharov.lesson_1.Room.entity.Employee2;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EntityKt;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EntityKt2;

@Dao
public abstract class EmployeeDao4 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(EntityKt employee2);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(EntityKt2 entityKt2);


    @Transaction
    public void transactionInsert(EntityKt employee2, EntityKt2 employee){
        insert(employee2);
        insert(employee);
    }

}
