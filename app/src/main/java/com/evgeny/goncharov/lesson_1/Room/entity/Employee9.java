package com.evgeny.goncharov.lesson_1.Room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;

@Entity
public class Employee9 {
    @PrimaryKey
    public long id;

    public String name;

    public int salary;

    //проигнорит это поле
    @Ignore
    public Bitmap avatar;

}
