package com.evgeny.goncharov.lesson_1.Room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


//Room имеет три основных компонента: Entity, Dao и Database.
@Entity
public class Employee2 {

    public Employee2(long id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    @PrimaryKey
    private long id;

    private String name;

    private int salary;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
