package com.evgeny.goncharov.lesson_1.Room.entity;


import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.evgeny.goncharov.lesson_1.Room.entity.Address8;


//Embedded подскажет Room, что надо
// просто взять поля из Address и считать их полями таблицы Employee.
@Entity
public class ArrEmployee6 {

    public ArrEmployee6(long id, String name, int salary, Address8 address) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.address = address;
    }

    @PrimaryKey(autoGenerate = true)
    public long id;

    public String name;

    public int salary;

    @Embedded
    public Address8 address;

}