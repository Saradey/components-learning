package com.evgeny.goncharov.lesson_1.Room.TypeСonverter;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.List;

@Entity
//@TypeConverters({HobbiesConverter.class})
public class Employee10 {

    @PrimaryKey
    public long id;

    public String name;

    public int salary;

    public long birthday;

    @TypeConverters({HobbiesConverter.class})
    public List<String> hobbies;

}
