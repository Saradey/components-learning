package com.evgeny.goncharov.lesson_1.interchangeBetweenFragmentsViewModel;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.evgeny.goncharov.lesson_1.R;

/**
 * Created by Evgeny Goncharov on 25/12/2018.
 * jtgn@yandex.ru
 */

public class MasterFragment extends Fragment {


    private SharedViewModel model;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        View rootView =
                inflater.inflate(R.layout.fragment1, container, false);


        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        Button button1 = rootView.findViewById(R.id.button1);



        button1.setOnClickListener(v ->{
            model.select("SSSS");
        });

        return rootView;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //itemSelector.setOnClickListener(item -> {

        //});

    }


}