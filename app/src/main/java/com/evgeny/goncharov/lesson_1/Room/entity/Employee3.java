package com.evgeny.goncharov.lesson_1.Room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ColumnInfo.INTEGER;

@Entity(tableName = "employees_thre")
public class Employee3 {

    public Employee3(String fullName, long id, int salary) {
        this.fullName = fullName;
        this.id = id;
        this.salary = salary;
    }

    // ...
    @ColumnInfo(name = "full_name")
    public String fullName;

    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(typeAffinity = INTEGER)
    public int salary;


}
