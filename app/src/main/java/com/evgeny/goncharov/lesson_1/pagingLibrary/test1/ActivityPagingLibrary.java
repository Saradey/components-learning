package com.evgeny.goncharov.lesson_1.pagingLibrary.test1;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.evgeny.goncharov.lesson_1.R;

import java.util.concurrent.Executors;

/**
 * Created by Evgeny Goncharov on 01/01/2019.
 * jtgn@yandex.ru
 */


public class ActivityPagingLibrary extends AppCompatActivity{


    private LiveData<PagedList<Employee>> posts;

    private EmployeeAdapter adapter;



    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_paging_library);

        // PagedList.Config - это конфиг PagedList.
        // В нем мы можем задать различные параметры, например, размер страницы.

        /*PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .build();*/

        //создание PagedList для RecycleView


//        // DataSource
//        MyPositionalDataSource dataSource = new MyPositionalDataSource(new EmployeeStorage());
//
//
//        // PagedList
//        PagedList.Config config = new PagedList.Config.Builder()
//                .setEnablePlaceholders(false)
//                .setPageSize(10)
//                .build();
//
//
//
//
//        MyDiffUtilCallback myDiffUtilCallback = new MyDiffUtilCallback();
//        // Adapter
//        adapter = new EmployeeAdapter(myDiffUtilCallback);
//
//
//        pagedList = new PagedList.Builder<>(dataSource, config)
//                .setFetchExecutor(Executors.newSingleThreadExecutor())
//                .setNotifyExecutor(new MainThreadExecutor())
//                .build();
//
//
//
//        adapter.submitList(pagedList);
//
//
//        // RecyclerView
//        RecyclerView recyclerView = findViewById(R.id.lizz);
//        recyclerView.setAdapter(adapter);
        MyDiffUtilCallback myDiffUtilCallback = new MyDiffUtilCallback();
        adapter = new EmployeeAdapter(myDiffUtilCallback);


        PagedList.Config pagedListConfig =
                new PagedList.Config.Builder().setEnablePlaceholders(false)
                        .setPrefetchDistance(10)
                        .setPageSize(10).build();

        // initial page size to fetch can also be configured here too
        PagedList.Config config = new PagedList.Config.Builder().setPageSize(20).build();

        ParseDataSourceFactory sourceFactory = new ParseDataSourceFactory();

        posts = new LivePagedListBuilder(sourceFactory, pagedListConfig)
                .build();


        posts.observe(this, data ->{
            adapter.submitList(data);
        });



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = findViewById(R.id.lizz);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

    }





}
