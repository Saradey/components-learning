package com.evgeny.goncharov.lesson_1.Room.TypeСonverter;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Dao
abstract public class Dao7 {

    @Query("SELECT * FROM Employee10 WHERE birthday = :birthday")
    abstract public Employee10 getByDate2(@TypeConverters({DateConverterQuery.class}) Date birthday);

    @Query("SELECT * FROM Employee10 WHERE birthday BETWEEN :birthdayFrom and :birthdayTo")
    @TypeConverters({DateConverterQuery.class})
    abstract public Employee10 getByDate(Date birthdayFrom, Date birthdayTo);
}


/*
@Dao
@TypeConverters({DateConverter.class})
public interface Dao7 {

   ...

}*/
