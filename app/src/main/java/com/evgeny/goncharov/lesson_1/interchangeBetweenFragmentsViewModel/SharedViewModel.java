package com.evgeny.goncharov.lesson_1.interchangeBetweenFragmentsViewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by Evgeny Goncharov on 25/12/2018.
 * jtgn@yandex.ru
 */


//перекидывание данных между фрагментами ы
public class SharedViewModel extends ViewModel {
    private final MutableLiveData<String> selected = new MutableLiveData<String>();

    public void select(String item) {
        selected.setValue(item);
    }

    public LiveData<String> getSelected() {
        return selected;
    }
}

