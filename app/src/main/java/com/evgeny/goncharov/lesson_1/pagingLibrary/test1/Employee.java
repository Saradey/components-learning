package com.evgeny.goncharov.lesson_1.pagingLibrary.test1;

/**
 * Created by Evgeny Goncharov on 01/01/2019.
 * jtgn@yandex.ru
 */

public class Employee {

    private String name = "";
    private double salary;
    private int age;
    private int id;

    public Employee(String name, double salary, int age, int id) {
        this.name = name;
        this.salary = salary;
        this.age = age;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        hashCode += hashCode * 1000 + 1;
        hashCode += age * 100 + 1;
        hashCode += id * 1000 + 1;
        hashCode += salary * 1000 + 1;
        return hashCode;
    }

}
