package com.evgeny.goncharov.lesson_1.Room.TypeСonverter;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class DateConverterQuery {

    @TypeConverter
    public Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }

}
