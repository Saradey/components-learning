package com.evgeny.goncharov.lesson_1.dataBinding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.evgeny.goncharov.lesson_1.R;
import com.evgeny.goncharov.lesson_1.databinding.ActivityDataBinding1Binding;

public class ActivityData1 extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_binding_1);


        Warrior warrior = new Warrior(1, 2, "Warrior", "Jafur");

        //Сразу хочу заметить, что если мы теперь в коде будем изменять
        // объект Employee, то данные на экране меняться не будут.
        // Они считались один раз и далее не отслеживаются
        // (при такой реализации).
        ActivityDataBinding1Binding binding =  DataBindingUtil.setContentView(this, R.layout.activity_data_binding_1);
        binding.setWarrior(warrior);


        warrior.setName("Sarad");
        warrior.setLizzz("AAA");
        binding.invalidateAll(); //Биндинг считает новые данные с
        // ранее полученного объекта Employee.

        //получение из биндинга
        TextView textViewName = binding.name;

        //Также можно получить корневое View методом getRoot:
        View rootView = binding.getRoot();




    }




}
