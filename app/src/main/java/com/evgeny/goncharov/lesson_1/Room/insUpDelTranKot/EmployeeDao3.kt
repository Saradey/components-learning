package com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Transaction


@Dao
abstract class EmployeeDao3 {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(entity : EntityKt)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(entity2: EntityKt2)


    //Аннотация @Transaction позволяет выполнять
    //несколько методов в рамках одной транзакции.
    //НЕ РАБОТАЕТ
   /* @Transaction
    fun insertTwoEntity(entity : EntityKt, entity2: EntityKt2){
        insert(entity)
        insert(entity2)
    }*/


}