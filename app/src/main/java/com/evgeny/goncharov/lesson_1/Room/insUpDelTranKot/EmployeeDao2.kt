package com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Update

@Dao
interface EmployeeDao2{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity : EntityKt)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entitys: MutableList<EntityKt>)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entitys: EntityKt2) : Long


    @Update
    fun update(entitys: EntityKt2)

    //не работает
    ///@Insert(onConflict = OnConflictStrategy.REPLACE)
    //fun insert(vararg entitys: EntityKt2)


}