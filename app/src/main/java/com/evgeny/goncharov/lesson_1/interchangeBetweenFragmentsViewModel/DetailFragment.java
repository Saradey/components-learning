package com.evgeny.goncharov.lesson_1.interchangeBetweenFragmentsViewModel;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evgeny.goncharov.lesson_1.R;

/**
 * Created by Evgeny Goncharov on 25/12/2018.
 * jtgn@yandex.ru
 */

public class DetailFragment extends Fragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        View rootView =
                inflater.inflate(R.layout.fragment2, container, false);

        TextView textView = rootView.findViewById(R.id.textViee1);


        SharedViewModel model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);


        model.getSelected().observe(this, textView::setText);

        return rootView;
    }

}

