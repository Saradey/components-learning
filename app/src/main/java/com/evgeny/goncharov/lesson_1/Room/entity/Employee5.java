package com.evgeny.goncharov.lesson_1.Room.entity;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

//внешние ключи
//Это означает, что при удалении родительского ключа,
// будут удалены, связанные с ним дочерние ключи.
@Entity(foreignKeys = @ForeignKey(entity = Employee2.class,
        parentColumns = "id",
        childColumns = "employee_id",
        onDelete = CASCADE))
public class Employee5 {


    public Employee5(long id2, String model, int year, long employeeId) {
        this.id2 = id2;
        this.model = model;
        this.year = year;
        this.employeeId = employeeId;
    }

    @PrimaryKey(autoGenerate = true)
    public long id2;

    public String model;

    public int year;

    @ColumnInfo(name = "employee_id")
    public long employeeId;

}
