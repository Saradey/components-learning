package com.evgeny.goncharov.lesson_1.pagingLibrary.test1;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.util.Log;


public class MyDiffUtilCallback extends DiffUtil.ItemCallback<Employee> {


    public MyDiffUtilCallback() {
        super();
    }


    @Override
    public boolean areItemsTheSame(@NonNull Employee employee, @NonNull Employee t1) {
        return employee.getId() == t1.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull Employee employee, @NonNull Employee t1) {
        return employee.hashCode() == t1.hashCode();
    }


}
