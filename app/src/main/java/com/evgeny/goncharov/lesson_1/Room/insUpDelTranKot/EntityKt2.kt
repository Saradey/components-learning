package com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.evgeny.goncharov.lesson_1.Room.query.Name


@Entity
data class EntityKt2(@PrimaryKey(autoGenerate = true) var id : Long?,
                var str : String){

    constructor() : this(null, "")

}