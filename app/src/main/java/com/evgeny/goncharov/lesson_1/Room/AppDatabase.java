package com.evgeny.goncharov.lesson_1.Room;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.evgeny.goncharov.lesson_1.Room.TypeСonverter.DateConverterQuery;
import com.evgeny.goncharov.lesson_1.Room.TypeСonverter.Employee10;
import com.evgeny.goncharov.lesson_1.Room.entity.ArrEmployee6;
import com.evgeny.goncharov.lesson_1.Room.entity.Car;
import com.evgeny.goncharov.lesson_1.Room.entity.CarDao;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee2;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee3;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee4;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee5;
import com.evgeny.goncharov.lesson_1.Room.entity.EmployeeDao;
import com.evgeny.goncharov.lesson_1.Room.entity.EmployeeDao4;
import com.evgeny.goncharov.lesson_1.Room.entity.IndexEmployee7;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EmployeeDao2;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EmployeeDao3;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EntityKt;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EntityKt2;
import com.evgeny.goncharov.lesson_1.Room.query.EmployeeDao5;
import com.evgeny.goncharov.lesson_1.Room.rxJavaRoom.Dao6;

//конвертир для все базы данных
/*@Database(entities = {Employee10.class}, version = 1)
@TypeConverters({DateConverterQuery.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract EmployeeDao employeeDao();
}*/


@Database(entities = {Employee2.class,
        Car.class,
        Employee3.class,
        Employee4.class,
        Employee5.class,
        ArrEmployee6.class,
        IndexEmployee7.class,
        EntityKt.class,
        EntityKt2.class},
        version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract EmployeeDao employeeDao();
    public abstract CarDao carDao();
    public abstract EmployeeDao2 getDAO2();
    public abstract EmployeeDao3 getDao3();
    public abstract EmployeeDao4 getDao4();
    public abstract EmployeeDao5 getDao5();
    public abstract Dao6 getDao6();
}


