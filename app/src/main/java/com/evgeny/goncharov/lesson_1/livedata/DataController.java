package com.evgeny.goncharov.lesson_1.livedata;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

/**
 * Created by Evgeny Goncharov on 22/12/2018.
 * jtgn@yandex.ru
 */


public class DataController {

    private static DataController dataController;

    private static MutableLiveData<String> liveData = new MutableLiveData<>();


    private DataController() {

    }


    public static synchronized DataController getInstance() {
        if (dataController == null)
            dataController = new DataController();
        return dataController;
    }


    public LiveData<String> getData() {
        return liveData;
    }

    public void setData(String str) {
        liveData.setValue(str);
    }

}
