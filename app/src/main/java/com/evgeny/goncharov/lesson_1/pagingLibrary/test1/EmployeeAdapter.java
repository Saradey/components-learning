package com.evgeny.goncharov.lesson_1.pagingLibrary.test1;

import android.annotation.SuppressLint;
import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evgeny.goncharov.lesson_1.R;

/**
 * Created by Evgeny Goncharov on 01/01/2019.
 * jtgn@yandex.ru
 */


//Как видите, он очень похож на RecyclerView.Adapter. От него также требуется биндить данные в Holder.
//
//Отличия следующие:
//
//1) Ему сразу надо предоставить DiffUtil.Callback. Если вы еще не знакомы с этой штукой, посмотрите мой материал.
//
//2) Нет никакого хранилища данных (List или т.п.)
//
//3) Нет метода getItemCount

class EmployeeAdapter extends PagedListAdapter<Employee, EmployeeAdapter.EmployeeViewHolder> {

    protected EmployeeAdapter(DiffUtil.ItemCallback<Employee> diffUtilCallback) {
        super(diffUtilCallback);
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.employee, parent, false);
        EmployeeViewHolder holder = new EmployeeViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        holder.bind(getItem(position));
    }


    class EmployeeViewHolder extends RecyclerView.ViewHolder{

        private TextView textView;

        public EmployeeViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.texsss);
        }


        @SuppressLint("SetTextI18n")
        public void bind(Employee employee){
            textView.setText(String.valueOf(employee.getId())+
                    " "+employee.getAge()+
                    " "+employee.getSalary());
        }

    }



}