package com.evgeny.goncharov.lesson_1.dataBinding;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class Warrior {

    private int attack;

    private int def;

    private String name;

    private String name2;

    private List<String> lizzz = new ArrayList<>();

    public Warrior(int attack, int def, String name, String name2) {
        this.attack = attack;
        this.def = def;
        this.name = name;
        this.name2 = name2;
    }



    public void onAttack(View view) {
        System.out.println("onAttack");
    }



    public void setLizzz(String string){
        lizzz.add(string);
    }

    public List<String> getLizzz() {
        return lizzz;
    }

    public void setLizzz(List<String> lizzz) {
        this.lizzz = lizzz;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
