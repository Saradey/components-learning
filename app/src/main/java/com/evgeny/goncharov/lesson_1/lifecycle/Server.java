package com.evgeny.goncharov.lesson_1.lifecycle;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.util.Log;

/**
 * Created by Evgeny Goncharov on 22/12/2018.
 * jtgn@yandex.ru
 */

public class Server implements LifecycleObserver{

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void connect(){
        Log.d("MyTag", "ON_START");
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void resume(){
        Log.d("MyTag", "ON_RESUME");
    }


}