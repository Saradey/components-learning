package com.evgeny.goncharov.lesson_1.Room.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

//Индексы могут повысить производительность вашей таблицы.
//Индекс дает возможность установить для его полей проверку на уникальность.
// Это делается параметром unique = true.
@Entity(indices = {
        @Index("salary"),
        @Index(value = {"first_name", "last_name"}, unique = true)
})
public class IndexEmployee7 {


    public IndexEmployee7(long id, String firstName, String lastName, int salary, int test) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.test = test;
    }

    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "first_name")
    public String firstName;

    @ColumnInfo(name = "last_name")
    public String lastName;

    public int salary;

    @ColumnInfo(index = true)
    public int test;

}