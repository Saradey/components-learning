package com.evgeny.goncharov.lesson_1.Room;


import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.evgeny.goncharov.lesson_1.App;
import com.evgeny.goncharov.lesson_1.R;
import com.evgeny.goncharov.lesson_1.Room.entity.Address8;
import com.evgeny.goncharov.lesson_1.Room.entity.ArrEmployee6;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee2;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee3;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee4;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee5;
import com.evgeny.goncharov.lesson_1.Room.entity.EmployeeDao;
import com.evgeny.goncharov.lesson_1.Room.entity.EmployeeDao4;
import com.evgeny.goncharov.lesson_1.Room.entity.IndexEmployee7;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EmployeeDao2;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EmployeeDao3;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EntityKt;
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EntityKt2;
import com.evgeny.goncharov.lesson_1.Room.query.EmployeeDao5;
import com.evgeny.goncharov.lesson_1.Room.query.Name;
import com.evgeny.goncharov.lesson_1.Room.rxJavaRoom.Dao6;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ActivityRoomMy extends AppCompatActivity {

    TextView textView;

    private EmployeeDao employeeDao;

    private EmployeeDao2 employeeDao2;

    private EmployeeDao3 employeeDao3;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_layout);

        textView = findViewById(R.id.textTSS);

        AppDatabase db = App.getAppDatabase();
        employeeDao = db.employeeDao();

        Employee2 employee = new Employee2(1, "SSS", 123);

        new Thread(new Runnable() {
            @Override
            public void run() {
                employeeDao.insert(employee);
            }
        }).start();


        Flowable.just(new Employee3("", 1, 11))
                .flatMap(flow -> {
                    return Flowable.just(flow)
                            .doOnNext(employeeDao::insert);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(repositori -> {
                    //textView.setText(String.valueOf(repositori.id));
                }, Throwable::printStackTrace);


        Flowable.just(new Employee3("", 1, 1))
                .doOnNext(employeeDao::insert)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(repositori -> {
                    //textView.setText(String.valueOf(repositori.id));
                });


        Observable.just(new Employee4(1, 2))
                .doOnNext(employeeDao::insert)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    //textView.setText(String.valueOf(data.key1));
                });


        Single.just(new Employee5(1, "", 1994, 1))
                .map(data -> {
                    employeeDao.insert(data);
                    return data;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    //textView.setText(String.valueOf(data.year));
                });


        Maybe.just(new ArrEmployee6(1, "", 111,
                new Address8("New-York", "Allen Street", 111)))
                .map(data -> {
                    employeeDao.insert(data);
                    return data;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data ->{
                    //textView.setText(data.address.city);
                });


        Completable.fromRunnable(this::run)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{
                   //textView.setText("Completable");
                });


        new Thread(this::run).start();

///////////////////котлин

        employeeDao2 = db.getDAO2();


        EntityKt entityKt = new EntityKt(1, "", 111);
        new Thread(new Runnable() {
            @Override
            public void run() {
                employeeDao2.insert(entityKt);
            }
        }).start();

        //textView.setText(String.valueOf(entityKt.getSalary()));

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<EntityKt> lizz = new ArrayList<>();
                lizz.add(new EntityKt(1, "", 111));
                lizz.add(new EntityKt(2, "", 222));
                employeeDao2.insert(lizz);
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                long teamp = 1;
                System.out.println(employeeDao2.insert(new EntityKt2( teamp, "")));
            }
        }).start();


        employeeDao3 = db.getDao3();

        /*db.runInTransaction(new Runnable() {
            @Override
            public void run() {
                //НЕ РАБОТАЕТ
                *//*employeeDao3.insertTwoEntity(
                        new EntityKt(1, "", 22),
                        new EntityKt2(3));*//*
            }
        });*/


        EmployeeDao4 employeeDao4 = db.getDao4();
        new Thread(new Runnable() {
            @Override
            public void run() {
                employeeDao4.transactionInsert(new EntityKt(1, "", 22),
                        new EntityKt2(1L, "Test"));
            }
        }).start();



        // Подписавшись на LiveData,
        // вы будете получать свежие данные при их изменении в базе.
        EmployeeDao5 employeeDao5 = db.getDao5();
        LiveData<EntityKt2> liveData = employeeDao5.getEntity(1L);


        liveData.observe(this, data->{
            //textView.setText(data.getStr());
        });


        new Thread(new Runnable() {
            @Override
            public void run() {
               //employeeDao5.update(new EntityKt2(1L, "Test2"));
            }
        }).start();



        new Thread(new Runnable() {
            @Override
            public void run() {
                /*employeeDao5.getNames();*/
            }
        }).start();




        //RXjava

        Dao6 dao6 = db.getDao6();


        //subscribeOn в случае с Flowable не нужен. Запрос в базу будет
        // выполнен не в UI потоке.
        // А вот, чтобы результат пришел в UI поток, используем observeOn
        dao6.getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Employee2>>() {
                    @Override
                    public void accept(List<Employee2> employees) throws Exception {
                        //textView.setText(String.valueOf(employees.get(0).getSalary()));
                    }
                });


        dao6.getById(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Employee2>() {
                    @Override
                    public void accept(Employee2 employee) throws Exception {
                        // ...
                    }
                });


        // Напомню, что в Single может прийти только один onNext,
        // либо OnError. После этого Single считается завершенным.
        //В отличие от Flowable, с Single необходимо использовать
        // onSubscribe, чтобы задать поток для выполнения запроса.
        dao6.getById2(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data->{

                }, t->{

                });


        // Напомню, что в Maybe может прийти либо один onNext, либо onComplete,
        // либо OnError. После этого Maybe считается завершенным.


        dao6.getById3(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableMaybeObserver<Employee2>() {
                    @Override
                    public void onSuccess(Employee2 employee) {
                        // ...
                    }

                    @Override
                    public void onError(Throwable e) {
                        // ...
                    }

                    @Override
                    public void onComplete() {
                        // ...
                    }
                });


        //Flowable подходит, если вы запрашиваете данные и далее
        // планируете автоматически получать их обновления.
        //Single и Maybe подходят для однократного получения данных.
        // Разница между ними в том, что Single логичнее использовать,
        // если запись должна быть в базе. Если ее нет, вам придет ошибка.
        // А Maybe допускает, что записи может и не быть.




        //Type Converter



    }


















    public void run(){
        employeeDao.insert(
                new IndexEmployee7(1, "SSS",
                        "SSS", 1000, 1));
    }


}
