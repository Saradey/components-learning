package com.evgeny.goncharov.lesson_1.Room.entity;


import android.arch.persistence.room.Entity;


//составные ключи
@Entity(primaryKeys = {"key1", "key2"})
public class Employee4 {

    public Employee4(long key1, long key2) {
        this.key1 = key1;
        this.key2 = key2;
    }

    public long key1;
    public long key2;

}
