package com.evgeny.goncharov.lesson_1.Room.query

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.evgeny.goncharov.lesson_1.Room.entity.Employee2
import com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot.EntityKt2




@Dao
abstract class EmployeeDao5 {


    @Query("SELECT * FROM EntityKt2")
    abstract fun getAll() : List<EntityKt2>

    //Синтаксический сахар
    @Query("SELECT * FROM EntityKt2 WHERE id = :id")
    abstract fun getEntity(id : Long) : LiveData<EntityKt2>

    @Update
    abstract fun update(value : EntityKt2)

    @Query("SELECT * FROM EntityKt2 WHERE id = :id")
    abstract fun getById(id : Long): EntityKt2

    @Query("SELECT * FROM Employee2 WHERE salary > :minSalary")
    abstract fun getAllWithSalaryMoreThan(minSalary: Int): List<Employee2>

    @Query("SELECT * FROM Employee2 WHERE salary BETWEEN :minSalary AND :maxSalary")
    abstract fun getAllWithSalaryBetween(minSalary: Int, maxSalary: Int): List<Employee2>

    //поиск сотрудников по списку id
    @Query("SELECT * FROM Employee2 WHERE id IN (:idList)")
    abstract fun getByIdList(idList: List<Long>): List<Employee2>


    /*@Query("SELECT first_name, last_name FROM EntityKt2")
    abstract fun getNames(): List<Name>*/

    //Аннотации Insert, Update и Delete позволяют нам модифицировать
    // данные, но их возможности слишком ограниченны. Часто возникает
    // необходимость обновить только некоторые поля или удалить записи
    // по определенному условию. Это можно сделать запросами с помощью
    // Query.

    @Query("UPDATE Employee2 SET salary = :newSalary WHERE id IN (:idList)")
    abstract fun updateSalaryByIdList(idList: List<Long>, newSalary: Int): Int

    @Query("DELETE from Employee2 WHERE id IN (:idList)")
    abstract fun deleteByIdList(idList: List<Long>): Int

}