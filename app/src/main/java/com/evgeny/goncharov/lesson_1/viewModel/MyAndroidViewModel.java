package com.evgeny.goncharov.lesson_1.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

/**
 * Created by Evgeny Goncharov on 22/12/2018.
 * jtgn@yandex.ru
 */

//Не стоит передавать Activity в модель в качестве Context.
// Это может привести к утечкам памяти.
//Если вам в модели понадобился объект Context, то вы можете наследовать
// не ViewModel, а AndroidViewModel.
public class MyAndroidViewModel extends AndroidViewModel{


    public MyAndroidViewModel(@NonNull Application application) {
        super(application);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
    }
}
