package com.evgeny.goncharov.lesson_1.livedata;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.evgeny.goncharov.lesson_1.R;

import java.util.function.Function;

/**
 * Created by Evgeny Goncharov on 22/12/2018.
 * jtgn@yandex.ru
 */

//Предполагается, что подписчиками LiveData будут Activity и фрагменты
// А их состояние активности будет определяться с помощью их Lifecycle объекта.
    //работает в связки с LifeCycle
public class LiveDataActivity extends AppCompatActivity{


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_data_activity);

        /*TextView textView = findViewById(R.id.mytext);
        TextView textView1 = findViewById(R.id.mytext2);

        //получаем контроллер
        DataController dataController = DataController.getInstance();
        LiveData<String> liveData = dataController.getData();
        liveData.observe(this, textView::setText);




        //мапнули данные из string в int
        LiveData<Integer> liveDataInt = Transformations.map(liveData, Integer::parseInt);

        Button button = findViewById(R.id.clicks);
        button.setOnClickListener(v -> {
            //Метод setValue должен быть вызван из UI потока.
            dataController.setData("1");
        });
*/

       /* //MediatorLiveData объядиняет результат двух MutableLiveData
        MutableLiveData<String> liveData1 = new MutableLiveData<>();
        MutableLiveData<String> liveData2 = new MutableLiveData<>();

        MediatorLiveData<String> mediatorLiveData = new MediatorLiveData<>();

        mediatorLiveData.addSource(liveData1, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                mediatorLiveData.setValue(s);
            }
        });

        mediatorLiveData.addSource(liveData2, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                mediatorLiveData.setValue(s);
            }
        });*/



        /*mediatorLiveData.observe(this, onChanged ->{
            Log.d("MyTag", "onChanged " + onChanged);
        });

        liveData1.setValue("1");
        liveData1.setValue("2");
        liveData2.setValue("3");
        liveData2.setValue("4");*/

        TextView textView = findViewById(R.id.mytext);

        textView.setText("2");

        MutableLiveData<String> liveData1 = new MutableLiveData<>();
        liveData1.observe(this, textView::setText);

        Button button = findViewById(R.id.clicks);
        button.setOnClickListener(v -> {
            //Метод setValue должен быть вызван из UI потока.
            liveData1.setValue("1");
        });



    }



}
