package com.evgeny.goncharov.lesson_1.Room.entity;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import com.evgeny.goncharov.lesson_1.Room.entity.ArrEmployee6;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee2;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee3;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee4;
import com.evgeny.goncharov.lesson_1.Room.entity.Employee5;
import com.evgeny.goncharov.lesson_1.Room.entity.IndexEmployee7;

import java.util.List;

//В объекте Dao мы будем описывать методы для работы с базой данных.
@Dao
public interface EmployeeDao {

    @Query("SELECT * FROM Employee2")
    List<Employee2> getAll1();

    @Query("SELECT * FROM Employee2 WHERE id = :id")
    Employee2 getById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Employee2 employee);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Employee2... employee);

    @Update
    void update(Employee2 employee);

    @Delete
    void delete(Employee2 employee);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Employee3 employee);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Employee4 employee);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Employee5 employee);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ArrEmployee6 employee);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(IndexEmployee7 employee);

    @Query("SELECT * FROM Employee2")
    Employee2[] getAll2();


    @Query("SELECT * FROM Employee2")
    Cursor getAll3();


}
