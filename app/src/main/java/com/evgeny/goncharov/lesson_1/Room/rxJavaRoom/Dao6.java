package com.evgeny.goncharov.lesson_1.Room.rxJavaRoom;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.evgeny.goncharov.lesson_1.Room.entity.Employee2;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
abstract public class Dao6 {

    @Query("SELECT * FROM Employee2")
    public abstract Flowable<List<Employee2>> getAll();

    @Query("SELECT * FROM Employee2 WHERE id = :id")
    public abstract Flowable<Employee2> getById(long id);

    @Query("SELECT * FROM Employee2 WHERE id = :id")
    public abstract Single<Employee2> getById2(long id);

    @Query("SELECT * FROM Employee2 WHERE id = :id")
    public abstract Maybe<Employee2> getById3(long id);

}
