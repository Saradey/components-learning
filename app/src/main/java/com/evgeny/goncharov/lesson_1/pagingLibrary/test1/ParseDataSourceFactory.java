package com.evgeny.goncharov.lesson_1.pagingLibrary.test1;

import android.arch.paging.DataSource;

public class ParseDataSourceFactory extends DataSource.Factory<Integer, Employee> {

    @Override
    public DataSource<Integer, Employee> create() {
        MyPositionalDataSource source = new MyPositionalDataSource(new EmployeeStorage());
        return source;
    }
}
