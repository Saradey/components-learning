package com.evgeny.goncharov.lesson_1;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.evgeny.goncharov.lesson_1.Room.AppDatabase;

public class App extends Application {

    private static AppDatabase appDatabase;


    @Override
    public void onCreate() {
        super.onCreate();
        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "sword")
                .build();
    }


    public static AppDatabase getAppDatabase() {
        return appDatabase;
    }

}
