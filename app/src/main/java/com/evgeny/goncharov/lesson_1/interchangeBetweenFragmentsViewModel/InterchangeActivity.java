package com.evgeny.goncharov.lesson_1.interchangeBetweenFragmentsViewModel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.evgeny.goncharov.lesson_1.R;

/**
 * Created by Evgeny Goncharov on 25/12/2018.
 * jtgn@yandex.ru
 */

public class InterchangeActivity extends AppCompatActivity{


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager transcation = getSupportFragmentManager();

        DetailFragment detailFragment = new DetailFragment();
        MasterFragment masterFragment = new MasterFragment();

        transcation.beginTransaction()
                .add(R.id.fil1, masterFragment)
                .add(R.id.fil2, detailFragment)
                .commit();
    }




}
