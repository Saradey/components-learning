package com.evgeny.goncharov.lesson_1.Room.insUpDelTranKot

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity
data class EntityKt(@PrimaryKey var id : Int,
                    var name : String,
                    var salary : Int){
}