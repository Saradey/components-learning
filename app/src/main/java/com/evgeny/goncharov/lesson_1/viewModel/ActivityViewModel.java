package com.evgeny.goncharov.lesson_1.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.evgeny.goncharov.lesson_1.R;

/**
 * Created by Evgeny Goncharov on 22/12/2018.
 * jtgn@yandex.ru
 */
public class ActivityViewModel extends AppCompatActivity{


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_data_activity);

        MyViewModel model = ViewModelProviders.of(this).get(MyViewModel.class);

        LiveData<String> data = model.getData();
        data.observe(this, v->{

        });






    }




}
