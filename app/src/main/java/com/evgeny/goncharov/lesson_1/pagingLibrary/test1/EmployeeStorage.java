package com.evgeny.goncharov.lesson_1.pagingLibrary.test1;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class EmployeeStorage {

    private List<Employee> lizz = new ArrayList<>();

    public EmployeeStorage(){
        for (int i = 0; i < 1000; i++){
            lizz.add(new Employee("", i*100, i +20 ,i));
        }
    }


    public List<Employee> getData(int startPosition, int loadSize) {
        Log.d("MyTag", String.valueOf(startPosition)+" "+String.valueOf(loadSize));
        return lizz.subList(startPosition, startPosition+loadSize);
    }


}
