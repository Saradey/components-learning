package com.evgeny.goncharov.lesson_1.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by Evgeny Goncharov on 22/12/2018.
 * jtgn@yandex.ru
 */

//ViewModel - здесь удобно держать все данные, которые нужны вам для формирования экрана.
// Они будут жить при поворотах экрана, но умрут, когда приложение будет убито системой.
//
//onSavedInstanceState - здесь нужно хранить тот минимум данных, который понадобится вам
// для восстановления состояния экрана и данных в ViewModel после экстренного закрытия
// Activity системой. Это может быть поисковый запрос, ID и т.п.
//
//
//
//Соответственно, когда вы достаете данные из savedInstanceState и предлагаете их модели,
// это может быть в двух случаях:
//
//1) Был обычный поворот экрана. В этом случае ваша модель должна понять,
// что ей эти данные не нужны, потому что при повороте экрана модель ничего не потеряла.
// И уж точно модель не должна заново делать запросы в БД, на сервер и т.п.
//
//2) Приложение было убито, и теперь запущено заново. В этом случае модель берет данные
// из savedInstanceState и использует их, чтобы восстановить свои данные. Например, берет
// ID и идет в БД за полными данными.
public class MyViewModel extends ViewModel {

    MutableLiveData<String> data;


    public LiveData<String> getData() {
        if (data == null) {
            data = new MutableLiveData<>();
            loadData();
        }
        return data;
    }



    private void loadData() {
        /*dataRepository.loadData(new Callback<String>() {
            @Override
            public void onLoad(String s) {
                data.postValue(s);
            }
        });*///получаем данные из какого то репозитория
    }



    @Override
    protected void onCleared() {
        // clean up resources
    }

}
