package com.evgeny.goncharov.lesson_1.lifecycle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.evgeny.goncharov.lesson_1.R;
import com.evgeny.goncharov.lesson_1.lifecycle.Server;

public class MainActivity extends AppCompatActivity {

    private Server server = new Server();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getLifecycle().addObserver(server);
    }








}
